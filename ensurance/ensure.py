import numpy as np


def ensure_equal_dims(to_check, names, func_name, dim=None):
    """
    Check that a set of arrays all have the same dimension. Raise an error with
    details if not.

    Parameters
    ----------
    to_check : list of arrays
        List of arrays to check for equal dimensions
    names : list
        List of variable names for arrays in to_check
    func_name : str
        Name of function calling ensure_equal_dims
    dim : int
        Integer index of specific axes to ensure shape of, default is to compare all dims

    Raises
    ------
    ValueError
        If any of the inputs in to_check have differing shapes

    """

    if dim is None:
        dim = np.arange(to_check[0].ndim)
    else:
        dim = [dim]

    all_dims = [tuple(np.array(x.shape)[dim]) for x in to_check]
    check = [True] + [all_dims[0] == all_dims[ii + 1] for ii in range(len(all_dims[1:]))]

    if np.alltrue(check) == False:  # noqa: E712
        msg = "Mismatch between inputs: "
        for ii in range(len(to_check)):
            msg += "'{0}': {1}, ".format(names[ii], to_check[ii].shape)
        raise ValueError(msg)


def ensure_vector(to_check, names, func_name):
    """
    Check that a set of arrays are all vectors with only 1-dimension. Arrays
    with singleton second dimensions will be trimmed and an error will be
    raised for non-singleton 2d or greater than 2d inputs.

    Parameters
    ----------
    to_check : list of arrays
        List of arrays to check for equal dimensions
    names : list
        List of variable names for arrays in to_check
    func_name : str
        Name of function calling ensure_equal_dims

    Returns
    -------
    out
        Copy of arrays in to_check with 1d shape.

    Raises
    ------
    ValueError
        If any input is a 2d or greater array

    """

    out_args = list(to_check)
    for idx, xx in enumerate(to_check):

        if (xx.ndim > 1) and (xx.shape[1] == 1):
            msg = "Checking {0} inputs - trimming singleton from input '{1}'"
            msg = msg.format(func_name, names[idx])
            out_args[idx] = out_args[idx][:, 0]
        elif (xx.ndim > 1) and (xx.shape[1] != 1):
            msg = "Checking {0} inputs - Input '{1}' {2} must be a vector or 2d with singleton second dim"
            msg = msg.format(func_name, names[idx], xx.shape)
            raise ValueError(msg)
        elif xx.ndim > 2:
            msg = "Checking {0} inputs - Shape of input '{1}' {2} must be a vector."
            msg = msg.format(func_name, names[idx], xx.shape)
            raise ValueError(msg)

    if len(out_args) == 1:
        return out_args[0]
    else:
        return out_args


def ensure_1d_with_singleton(to_check, names, func_name):
    """
    Check that a set of arrays are all vectors with a singleton second
    dimneions. 1d arrays will have a singleton second dimension added and an
    error will be raised for non-singleton 2d or greater than 2d inputs.

    Parameters
    ----------
    to_check : list of arrays
        List of arrays to check for equal dimensions
    names : list
        List of variable names for arrays in to_check
    func_name : str
        Name of function calling ensure_equal_dims

    Returns
    -------
    out
        Copy of arrays in to_check with '1d with singleton' shape.

    Raises
    ------
    ValueError
        If any input is a 2d or greater array

    """

    out_args = list(to_check)
    for idx, xx in enumerate(to_check):

        if (xx.ndim > 2) and np.all(xx.shape[1:] == np.ones_like(xx.shape[1:])):
            # nd input where all trailing are ones
            msg = "Checking {0} inputs - Trimming trailing singletons from input '{1}' (input size {2})"
            out_args[idx] = np.squeeze(xx)[:, np.newaxis]
        if (xx.ndim > 2) and np.all(xx.shape[1:] == np.ones_like(xx.shape[1:])) == False:  # noqa: E712
            # nd input where some trailing are not one
            msg = "Checking {0} inputs - trailing dims of input '{1}' {2} must be singletons (length=1)"
            raise ValueError(msg)
        elif xx.ndim == 1:
            # Vector input - add a dummy dimension
            msg = "Checking {0} inputs - Adding dummy dimension to input '{1}'"
            out_args[idx] = out_args[idx][:, np.newaxis]

    if len(out_args) == 1:
        return out_args[0]
    else:
        return out_args


def ensure_2d(to_check, names, func_name):
    """
    Check that a set of arrays are all arrays with 2 dimensions. 1d arrays will
    have a singleton second dimension added.

    Parameters
    ----------
    to_check : list of arrays
        List of arrays to check for equal dimensions
    names : list
        List of variable names for arrays in to_check
    func_name : str
        Name of function calling ensure_equal_dims

    Returns
    -------
    out
        Copy of arrays in to_check with 2d shape.

    """

    out_args = list(to_check)
    for idx in range(len(to_check)):

        if to_check[idx].ndim == 1:
            msg = "Checking {0} inputs - Adding dummy dimension to input '{1}'"
            out_args[idx] = out_args[idx][:, np.newaxis]

    if len(out_args) == 1:
        return out_args[0]
    else:
        return out_args

if __name__ == '__main__':
    import inspect
    def retrieve_name(var):
        callers_local_vars = inspect.currentframe().f_back.f_locals.items()
        return [var_name for var_name, var_val in callers_local_vars if var_val is var]

    def get_positional_names(func):
        sig = inspect.signature(func)
        pos_names = []
        for p in sig.parameters:
            param = sig.parameters[p]
            if param.default == inspect._empty:
                pos_names.append(param.name)
        return pos_names

    # Actual Ensurance function
    def _ensure_boolean_array(X, name=None):
        if name is None:
            name = retrieve_name(X)
        if np.can_cast(X.dtype, np.bool, casting='safe'):
            print('input {0} is bool-able'.format(name))
            return X.astype(np.bool)
        else:
            raise ValueError('cannot safely cast {0} to boolean dtype'.format(name))

    def _ensure_2d_array(X, name=None):
        if name is None:
            name = retrieve_name(X)
        Y = X.copy()
        if X.ndim == 1:
            Y = Y[:, np.newaxis]
            print('adding dummy axis to {0}'.format(name))
        elif X.ndim > 2:
            raise ValueError('input size >2d {0}'.format(name))
        else:
            print('all good with {0}'.format(name))
            pass
        return Y

    def _ensure_equal_dims(all_dims, names=None, dim=None):
        "Takes list of dim sizes"
        if names is None:
            names = ['Input:' + str(ii) for ii in range(len(all_dims))]
        check = [True] + [all_dims[0] == all_dims[ii + 1] for ii in range(len(all_dims[1:]))]
        if np.alltrue(check) == False:  # noqa: E712
            msg = "Mismatch between inputs: "
            for ii in range(len(all_dims)):
                msg += "'{0}': {1}, ".format(names[ii], all_dims[ii])
            raise ValueError(msg)
        else:
            msg = "dims match between inputs: "
            for ii in range(len(all_dims)):
                msg += "'{0}', ".format(names[ii])
            print(msg)

    # Wrapper calling ensurance function on specified args and kwargs
    def _preproc_checks(check_args, check_kwargs):
        if isinstance(check_args, int):
            check_args = [check_args]
        elif check_args is None:
            check_args = []
        if isinstance(check_kwargs, str):
            check_kwargs = [check_kwargs]
        elif check_kwargs is None:
            check_kwargs = {}
        return check_args, check_kwargs

    import wrapt
    def ensure_boolean_array(*, check_args, check_kwargs):
        check_args, check_kwargs = _preproc_checks(check_args, check_kwargs)

        @wrapt.decorator
        def wrapper(wrapped, instance, args, kwargs):
            print('Ensurance evaluation for {0}'.format(wrapped.__name__))
            pos_names = get_positional_names(wrapped)
            out_args = list(args)
            for ii in check_args:
                out_args[ii] = _ensure_boolean_array(args[ii], name=pos_names[ii])

            out_kwargs = kwargs.copy()
            for key in check_kwargs:
                out_kwargs[key] = _ensure_boolean_array(kwargs[key], name=key)

            return wrapped(*out_args, **out_kwargs)
        return wrapper

    def ensure_2d_array(*, check_args, check_kwargs):
        check_args, check_kwargs = _preproc_checks(check_args, check_kwargs)

        @wrapt.decorator
        def wrapper(wrapped, instance, args, kwargs):
            print('Ensurance evaluation for {0}'.format(wrapped.__name__))
            pos_names = get_positional_names(wrapped)
            out_args = list(args)
            for ii in check_args:
                out_args[ii] = _ensure_2d_array(args[ii], name=pos_names[ii])

            out_kwargs = kwargs.copy()
            for key in check_kwargs:
                out_kwargs[key] = _ensure_2d_array(kwargs[key], name=key)

            return wrapped(*out_args, **out_kwargs)
        return wrapper

    def ensure_equal_dims(*, check_args, check_kwargs):
        check_args, check_kwargs = _preproc_checks(check_args, check_kwargs)

        @wrapt.decorator
        def wrapper(wrapped, instance, args, kwargs):
            print('Ensurance evaluation for {0}'.format(wrapped.__name__))
            pos_names = get_positional_names(wrapped)
            all_dims = []
            names = []
            for ii in check_args:
                all_dims.append(args[ii].shape)
                names.append(pos_names[ii])

            for key in check_kwargs:
                all_dims.append(kwargs[key].shape)
                names.append(key)

            _ensure_equal_dims(all_dims, names=names)

            return #wrapped(*out_args, **out_kwargs)
        return wrapper

    # ----- Dev sees the following

    # Wrapt'd function
    @ensure_2d_array(check_args=[0, 1], check_kwargs='z')
    @ensure_boolean_array(check_args=None, check_kwargs='z')
    @ensure_equal_dims(check_args=1, check_kwargs='z')
    def my_func(data, covariate, z=3):
        return data / 3

    # ---- User sees the following

    # Example
    X = np.arange(16).reshape(4,4)
    Y = np.arange(3)
    Z = np.arange(3) > 1

    my_func(X, Y, z=Z)
